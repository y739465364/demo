USE [StudentCourseRZ1149]
GO
/****** Object:  Table [dbo].[StudentsRZ1149]    Script Date: 2018/11/1 19:54:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentsRZ1149](
	[Sno] [nchar](10) NOT NULL,
	[Sname] [nvarchar](20) NOT NULL,
	[Ssex] [nchar](1) NOT NULL CONSTRAINT [DF_Students_Ssex]  DEFAULT (N'男'),
	[Sbirthday] [smalldatetime] NULL,
	[Sdept] [nvarchar](20) NULL,
	[Memo] [text] NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
	[Sno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'060001    ', N'赵林林', N'男', CAST(N'1985-09-08 00:00:00' AS SmallDateTime), N'机电系', N'test1')
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'060100    ', N'张修雨', N'女', CAST(N'2018-10-02 00:00:00' AS SmallDateTime), N'22', N'test2')
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'060101    ', N'钟文辉', N'男', CAST(N'2018-10-18 00:00:00' AS SmallDateTime), N'计算机系', N'优秀毕业生')
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'060102    ', N'吴细文', N'女', CAST(N'1997-03-24 00:00:00' AS SmallDateTime), N'计算机系', N'爱好：音乐')
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'060103    ', N'吴朝西', N'男', CAST(N'1998-07-01 00:00:00' AS SmallDateTime), N'某系', N'11')
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'070101    ', N'王冲瑞', N'男', CAST(N'1998-05-04 00:00:00' AS SmallDateTime), N'机电系', N'爱好：音乐')
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'070102    ', N'林滔滔', N'女', CAST(N'1997-04-03 00:00:00' AS SmallDateTime), N'机电系', N'爱好：体育')
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'070103    ', N'李修雨', N'女', CAST(N'1996-03-03 00:00:00' AS SmallDateTime), N'机电系', N'changed')
INSERT [dbo].[StudentsRZ1149] ([Sno], [Sname], [Ssex], [Sbirthday], [Sdept], [Memo]) VALUES (N'070301    ', N'李奇', N'男', CAST(N'1998-09-17 00:00:00' AS SmallDateTime), N'信息管理系', NULL)
ALTER TABLE [dbo].[StudentsRZ1149]  WITH CHECK ADD  CONSTRAINT [CK_Students] CHECK  (([Ssex]='男' OR [Ssex]='女'))
GO
ALTER TABLE [dbo].[StudentsRZ1149] CHECK CONSTRAINT [CK_Students]
GO
