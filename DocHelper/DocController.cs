﻿using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DocHelper
{
    public class DocController
    {

        private string _documentPath;
        static private RichEditControl _richEditControl = new RichEditControl();

        public RichEditControl RichEditControl { get => _richEditControl; set => _richEditControl = value; }

        public DocController(string documentPath)
        {

            OpenDoc(documentPath);
        }

        public void SaveToFile(string filename)
        {
            _richEditControl.SaveDocument(filename, DocumentFormat.Doc);

        }

        public void SetText(string bookmark, string value)
        {
            var document = _richEditControl.Document;
            document.BeginUpdate();
            try
            {
                DocumentRange range = _richEditControl.Document.Bookmarks[bookmark].Range;
                document.Replace(range, value);
            }
            finally
            {
                document.EndUpdate();
            }
        }

        public void OpenDoc(string documentPath)
        {
            _documentPath = documentPath;
            _richEditControl.CreateNewDocument();

            using (Stream stream = new FileStream(documentPath, FileMode.Open, FileAccess.Read))
            {
                stream.Seek(0, SeekOrigin.Begin);
                _richEditControl.LoadDocument(stream, DocumentFormat.Doc);
                stream.Close();
            }
        }
    }
}
