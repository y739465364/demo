﻿using System.Data;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars.Ribbon;
using DevExpress.Spreadsheet;
using DocHelper;
using System.Windows.Forms;

namespace DXApplication5
{
    public partial class Form1 : RibbonForm
    {
        IWorkbook workbook;
        DataView dataView;

        /// <summary>
        /// 总流程：
        /// 1.新建表模板(excel文件)：可以在excel建好，也可以在spreadsheet中建好再导出到excel文件
        /// 2.从excel文件读取表模板：LoadDocument
        /// 3.添加数据BindToDataSource()
        /// 4.自带的saveAs，导出到excel
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            InitSkinGallery();
            
            workbook = spreadsheetControl.Document;
            workbook.LoadDocument(@"Documents\1.xlsx", DocumentFormat.Xlsx);
            CreateDataSource();
            BindToDataSource();
        }

        /// <summary>
        /// 1.将dataView绑定到worksheet
        /// 2.(4,1)表示从第5行第2列开始填充数据
        /// </summary>
        private void BindToDataSource()
        {
            Worksheet worksheet = workbook.Worksheets[0];
            worksheet.DataBindings.BindToDataSource(dataView, 4, 1);
        }

        /// <summary>
        /// 创建带数据的dataView：
        /// 1.右键项目添加项：数据集dataSet
        /// 2.新建数据连接，将要导入数据的表直接拖到dataSet中
        /// </summary>
        private void CreateDataSource()
        {
            DataSet1 dataSet = new DataSet1();
            DataSet1TableAdapters.StudentsRZ1149TableAdapter tableAdapter = new DataSet1TableAdapters.StudentsRZ1149TableAdapter();
            tableAdapter.Fill(dataSet.StudentsRZ1149);
            dataView = new DataView(dataSet.StudentsRZ1149);
        }

        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();

            DocController doc = new DocController(ofd.FileName);

            doc.SetText("Content", @"新京报讯 湖南宁乡李女士哥哥的尸体10月30日被放在湖南宁乡市人民医院的太平间，几小时后尸体两只眼睛都被挖走。今日(11月1日），新京报记者从宁乡市委宣传部获悉，截至10月31日17时，共有三名犯罪嫌疑人被警方控制，目前案件正在进一步侦办之中。

新京报此前报道，李女士的哥哥10月30日早去世，家人和院方联系后将尸体放到宁乡市人民医院太平间。当天下午6点多，家人发现冰棺里尸体两只眼睛被挖。");
            doc.SetText("ModelType", "Empire VI");
            doc.SetText("Number", "000001");

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.ShowDialog();

            doc.SaveToFile(sfd.FileName);
        }
    }
}